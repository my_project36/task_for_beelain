import os
import pytest
from pathlib import Path
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.fixture()
def RoleSysVars(host):
    path = Path(os.environ["MOLECULE_PROJECT_DIRECTORY"], "vars")

    facts = host.ansible("setup")["ansible_facts"]

    for sys in ["ansible_distribution", "ansible_os_family"]:
        sys_file = path.joinpath(f"sys_{facts[sys]}.yml")
        if sys_file.exists():
            inc = host.ansible("include_vars", f"file={sys_file}")
            return inc["ansible_facts"]

    raise Exception("Cannot load role system variables")


def test_service(host, RoleSysVars):
    srv = host.service(RoleSysVars["pg_service"])
    assert srv.is_running
    assert srv.is_enabled


def test_socket(host):
    host.socket("tcp://5432").is_listening
